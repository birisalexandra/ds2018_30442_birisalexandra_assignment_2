import interfaces.PriceInterface;
import interfaces.TaxInterface;
import services.PriceService;
import services.TaxService;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServerMain {

    public static void main(String[] args) {

        try{
            TaxService taxService = new TaxService();
            TaxInterface stub = (TaxInterface) UnicastRemoteObject.exportObject(taxService, 0);

            PriceService priceService = new PriceService();
            PriceInterface stub2 = (PriceInterface) UnicastRemoteObject.exportObject(priceService, 0);

            Registry registry = LocateRegistry.createRegistry(1080);
            registry.bind("tax", stub);
            registry.bind("price", stub2);
        }catch (RemoteException e) {
            e.printStackTrace();
        } catch (AlreadyBoundException e) {
            e.printStackTrace();
        }
    }
}
