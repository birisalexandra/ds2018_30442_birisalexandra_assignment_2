package services;

import entities.Car;
import interfaces.TaxInterface;

import java.rmi.RemoteException;

public class TaxService implements TaxInterface {

    public double computeTax(Car c) throws RemoteException {
        if (c.getEngineSize() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }

        int sum = 8;
        if(c.getEngineSize() > 1601) sum = 18;
        if(c.getEngineSize() > 2001) sum = 72;
        if(c.getEngineSize() > 2601) sum = 144;
        if(c.getEngineSize() > 3001) sum = 290;
        return c.getEngineSize() / 200.0 * sum;
    }
}
