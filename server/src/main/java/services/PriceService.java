package services;

import entities.Car;
import interfaces.PriceInterface;

import java.rmi.RemoteException;

public class PriceService implements PriceInterface {

    public double computePrice(Car c) throws RemoteException {
        if (c.getYear() > 2018) {
            throw new IllegalArgumentException("Purchasing year can not be higher than the current year!");
        }

        if (2018 - c.getYear() < 7)
            return c.getPrice() - ((c.getPrice() / 7 ) * (2018 - c.getYear()));
        return 0;
    }
}
