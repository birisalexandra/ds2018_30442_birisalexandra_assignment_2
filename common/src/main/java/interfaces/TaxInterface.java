package interfaces;

import entities.Car;

public interface TaxInterface extends java.rmi.Remote {

    double computeTax(Car c) throws java.rmi.RemoteException;
}
