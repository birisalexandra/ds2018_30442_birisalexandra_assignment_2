package interfaces;

import entities.Car;

public interface PriceInterface extends java.rmi.Remote {

    double computePrice(Car c) throws java.rmi.RemoteException;
}
