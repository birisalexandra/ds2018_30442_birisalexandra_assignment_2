package controller;

import entities.Car;
import interfaces.PriceInterface;
import interfaces.TaxInterface;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ClientController {

    @FXML
    private Button tax;

    @FXML
    private Button sellingPrice;

    @FXML
    private TextField year;

    @FXML
    private TextField engine;

    @FXML
    private TextField price;

    @FXML
    private Label message;

    public ClientController() {}

    public void handleButtonTax(javafx.event.ActionEvent event) throws RemoteException {
        try {
            Registry registry = LocateRegistry.getRegistry(1080);
            TaxInterface service = (TaxInterface) registry.lookup("tax");
            double tax = service.computeTax(new Car(Integer.parseInt(year.getText()), Integer.parseInt(engine.getText()), Double.parseDouble(price.getText())));
            message.setText("The car's tax is: " + tax);
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }

    public void handleButtonSellingPrice(javafx.event.ActionEvent event) throws RemoteException {
        try {
            Registry registry = LocateRegistry.getRegistry(1080);
            PriceInterface service = (PriceInterface) registry.lookup("price");
            double sellPrice = service.computePrice(new Car(Integer.parseInt(year.getText()), Integer.parseInt(engine.getText()), Double.parseDouble(price.getText())));
            message.setText("The car's selling price is: " + sellPrice);
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }
}